import sys
import requests
from bs4 import BeautifulSoup
from gtts import gTTS

def speech_synthesis_from_web(url):
    # Mendapatkan response dari halaman web
    response = requests.get(url)

    # Parsing konten dengan Beautiful Soup
    soup = BeautifulSoup(response.content, "html.parser")

    # Menemukan elemen-elemen teks yang ingin dibaca
    # Di sini, contohnya adalah menemukan semua elemen <p>
    teks = ""
    paragraphs = soup.find_all("p")
    for paragraph in paragraphs:
        teks += paragraph.get_text() + " "

    # Mendapatkan bahasa dari header response
    bahasa = response.headers.get("content-language")
    if bahasa is None:
        bahasa = "en"  # Jika bahasa tidak tersedia, default ke bahasa Inggris

    # Membuat speech synthesis dengan gTTS dan menyesuaikan suara dengan bahasa web
    tts = gTTS(teks, lang=bahasa, slow=False)


    # Menyimpan speech synthesis ke dalam file
    tts.save("output.mp3") #output audio berbahasa indonesia -> url (https://id.wikipedia.org/wiki/Sejarah_Indonesia)
    #tts.save("output-2.mp3") #output berbahasa inggris -> url (https://en.wikipedia.org/wiki/Main_Page)

# Mengambil URL dari argumen command-line
if len(sys.argv) < 2:
    print("Usage: python program.py <url>")
    sys.exit(1)

url = sys.argv[1]
speech_synthesis_from_web(url)


